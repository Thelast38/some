import { Controller, Get, Req, Res, Post, Body, Render } from '@nestjs/common';
import {Response, Request} from 'express';
import {ApiDateReactor, RunId, ApiData, UserData, LogID} from '../dtos/usersData.dto';
import {CreatePdfService} from "../services/createpdf.service";
import { messages } from '@nestjs/core/constants';
import { Log } from '../entities/log.entity';
import { getManager } from 'typeorm';
import { Run } from '../entities/run.entity';
import { User } from '../entities/user.entity';
import { SubSystem } from '../entities/sub_system.entity';
@Controller('createpdf')
export class CreatepdfController {
  constructor(public createPdfService: CreatePdfService){};

  @Get('useractivity')
  async getUserActivity(@Req() req:Request, @Res() res: Response): Promise<void>{
    var allDate = this.createPdfService.getDate();

    let id: LogID;
    id = req.query;
    const repository = getManager().getRepository(Log);
    console.log(id.id);
    const result = await repository.createQueryBuilder("log")
      .leftJoinAndSelect("log.user", "user_id, external_id, sams_id")
      .where("log.log_id = :id", {id: id.id})
      .getOne();
    console.log(result);
    let params = {user:result["user"]["userId"], external:result["user"]["externalUserId"], sams:result["user"]["samsId"], logId:result["logId"],
    subsytemID:result["subsystemFkSubsystemId"], subtype:result["subtype"], origin:result["origin"], creationTime:result["creationTime"],
    title:result["title"], text:result["text"]};
    return this.createPdfService.createPdf(params, `User's Activity ${allDate}.pdf`, "/templates/UsersActivityTemplate.ejs", res);   
  }

  @Get('runconf')
  async runconf(@Req() req:Request, @Res() res: Response): Promise<void>{
    var allDate = this.createPdfService.getDate();
   let number = req.query;
   const repository = getManager().getRepository(Run);
    console.log(number);
  const result = await repository.createQueryBuilder("run")
    .where("run.run_number = :number",{number: number.number1})
    .getOne();
    const result2 = await repository.createQueryBuilder("run")
    .where("run.run_number = :number",{number: number.number2})
    .getOne();
    let params={Run1: result["runNumber"],O21: result["timeO2Start"],TargetStart1: result["timeTrgStart"],TargetEnd1: result["timeTrgEnd"],
    o2End1:result["timeO2End"],runType1:result["runType"],RunQuality1: result["runQuality"],NumbDetectors1: result["nDetectors"],
    numberFlips1: result["nFlps"],timeFrame1:result["nTimeframes"],SubTimeFrame1:result["nSubtimeframes"],Readout1: result["bytesReadOut"],
    Bytestimeframe1: result["bytesTimeframeBuilder"],
    Run2: result2["runNumber"],O22: result2["timeO2Start"],TargetStart2: result2["timeTrgStart"],TargetEnd2: result2["timeTrgEnd"],
    o2End2:result2["timeO2End"],runType2:result2["runType"],RunQuality2: result2["runQuality"],NumbDetectors2: result["nDetectors"],
    numberFlips2: result2["nFlps"],timeFrame2:result2["nTimeframes"],SubTimeFrame2:result2["nSubtimeframes"],Readout2: result2["bytesReadOut"],
    Bytestimeframe2: result2["bytesTimeframeBuilder"],
    Data:allDate
    
    }
    console.log(params);
    return this.createPdfService.createPdf(params, `Run ${allDate}`, "/templates/Run.ejs", res);  
   
  }
>>>>>>> bea4d8dda5790c8e44783e637ca171ef96bc8451



  @Get('runInf')
  async runInf(@Req() req:Request, @Res() res: Response) :Promise<void>{
    
    var allDate = this.createPdfService.getDate();
    let id: RunId;
    id = req.query;
    let params;
    /*let str:string;
    for (var i = 0; i < id.id.length;i++ ){
      if(i == 0){
        str = id.id[i];
      } else {
        str += "," + id.id[i];
      }
      
    }*/
    const repository = getManager().getRepository(Run);

    

    const result = await repository.findOne({runNumber:1});

    console.log(result);

    //params = {run_id : run_id, time_o2_start: time_o2_start, time_o2_end: time_o2_end, time_trg_start:time_trg_start, time_trg_end:time_trg_end, date: allDate};

    
    
        
  }

  @Post('downloadPDF')
  makePdf(@Res() res: Response, @Body() userData: UserData): void {
    
    var allDate = this.createPdfService.getDate();
    let fname = userData.fname;
    let lname = userData.lname;
    let occupation = userData.occupation;
    let bugReport = userData.bugReport;
    console.log(userData);
    const params = {firstname : fname, lastname: lname, occupation: occupation, bugReport:bugReport, date:allDate};
    return this.createPdfService.createPdf(params, `BugReport_${allDate}.pdf`, "/template.ejs", res);   
   }

   @Get()
   @Render("index")
   func(){
  
   return {};
   }
  
   @Get('apiname')
  api(@Req() req:Request, @Res() res: Response): void {
    
    var allDate = this.createPdfService.getDate();
    var temper: ApiData;
    temper = req.query;
  console.log(temper);
  var data = temper.data;
  var temp = temper.temp;
  const params = {data: data ,temp: temp};
  return this.createPdfService.createPdf(params, `BugReport_${allDate}.pdf`, "/get1_template.ejs", res);
  }


  @Get('reactorInfo')
  reactorInfo(@Req() req:Request, @Res() res: Response): void {
  var date: ApiDateReactor;
  date = req.query;
  let temp:number[] = new Array(4);
  let lnames:string[] = ["Smith", "Johnson", "Williams", "Jones", 
  "Brown", "Davies", "Miler", "Wilson", "Moore", "Taylor", "Jones", 
  "Thomas", "Evans", "White", "Anderson", "Martin", "Loe", "Hope", "Crowd", "Young"];
  let fnames:string[] = ["Ethan", "Kevin", "Justin", "Matthew", 
  "William", "Christopher", "Anthony", "Ryan", "Nicholas", "David", "Alex", 
  "James", "Josh", "Dilon", "Brandon", "Philip", "Fred", "Tyler", "Thomas", "Caleb"];
  console.log(date);
  
  for (var i = 0; i < 4; i++){
        var a = this.createPdfService.getRandomInt(200, 900);
        temp[i] = this.createPdfService.getRandomInt(200, 900);
        console.log(a);
  }
  console.log(temp);
  var worker1: string = fnames[this.createPdfService.getRandomInt(0, 19)] + " " + lnames[this.createPdfService.getRandomInt(0, 19)];
  var worker2: string = fnames[this.createPdfService.getRandomInt(0, 19)] + " " + lnames[this.createPdfService.getRandomInt(0, 19)];
  var visitor: string = fnames[this.createPdfService.getRandomInt(0, 19)] + " " + lnames[this.createPdfService.getRandomInt(0, 19)];
  var chief: string = fnames[this.createPdfService.getRandomInt(0, 19)] + " " + lnames[this.createPdfService.getRandomInt(0, 19)];
  var occupationChief: string = "Chief engineer"; 
  var bugReport: string = "No reports";
  const params = {date: date.date, temp: temp, worker1: worker1, worker2:worker2, occupation1:"Student", occupation2:"Student", visitor:visitor, chiefName:chief, chiefOccupation:occupationChief, bugReport:bugReport};
  return this.createPdfService.createPdf(params, `RunInfo_${date.date}.pdf`, "/11.ejs", res);
  }


  @Get('jobEvaluation')
  jobEv(@Req() req:Request, @Res() res: Response): void {
  
  var date: ApiDateReactor;
  date = req.query;
  
  let lnames:string[] = ["Smith", "Johnson", "Williams", "Jones", 
  "Brown", "Davies", "Miler", "Wilson", "Moore", "Taylor", "Jones", 
  "Thomas", "Evans", "White", "Anderson", "Martin", "Loe", "Hope", "Crowd", "Young"];
  let fnames:string[] = ["Ethan", "Kevin", "Justin", "Matthew", 
  "William", "Christopher", "Anthony", "Ryan", "Nicholas", "David", "Alex", 
  "James", "Josh", "Dilon", "Brandon", "Philip", "Fred", "Tyler", "Thomas", "Caleb"];
  let worker:string[] = new Array(13);
  let name: string;
  let name2:string;
  console.log(date);
  for (var i = 0; i < 13; i++){
      var a = this.createPdfService.getRandomInt(0, 19);
      name = fnames[a];
      a = this.createPdfService.getRandomInt(0, 19);  
      name2 = lnames[a];
      worker[i] = name + " " + name2;
  }
  let chief: string = fnames[this.createPdfService.getRandomInt(0, 19)] + " " + lnames[this.createPdfService.getRandomInt(0, 19)];
 
  
  
  
  
  const params = {date: date.date, worker: worker, Chief:chief};
  return this.createPdfService.createPdf(params, `RunInfo_${date.date}.pdf`, "/12.ejs", res);
  }
}
