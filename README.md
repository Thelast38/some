## Project: "Open International Project "jiskefet". API development for making Tech work-shift reports with a template"

Final version of the project is using nestjs for the server, and ejs + html-pdf for generating pdf, however now typeORM is being used instead of mysql library is used for data selection.

## Participants
| Study group | Username | Fullname |
|----------------|------------------|--------------------------| 
| 181-351 | @Shadthrough | Bogachev M. G. |
| 181-351 | @TheLast38 | Semennikov K.V. |
| 181-351 | @Anton250 | Petrov A.D. |

## Participants' personal contribution

### Bogachev M. G.

- Enchanted site design;
- Upgraded PDF-report templates;
- Added documentation for git;

### Semennikov K.V.

- Made a part of data downloading from database through typeORM;

### Petrov A.D.

- Wrote the nestjs server;
- Made API-Requests;
- Made a part of data downloading from database through typeORM;
- Embedded our project into Jiskefets and made swagger documentation;
- Connected additional htmls;

## Проект: «Международный открытый проект jiskefet. Разработка API для создания отчётов технических смен по шаблону»

Финальная версия проекта также использует библиотеку nestjs для сервера, ejs и html-pdf для генерации pdf, теперь вместо mysql используется библиотека typeORM для выгрузки данных из базы.

## Участники

| Учебная группа | Имя пользователя | ФИО                      |
|----------------|------------------|--------------------------|
| 181-351       | @Shadthrough       | Богачев М.Г.              |
| 181-351        | @TheLast38       | Семенников К.В.              |
| 181-351        | @Anton250      | Петров А.Д. |

## Личный вклад участников

### Богачев М.Г.

- Улучшил дизайн сайт дизайн сайта;
- Доработал шаблоны для pdf-отчётов;
- Сделал документацию для git;

### Семенников К.В.

- Сделал часть выгрузки из базы данных через typeORM;

### Петров А.Д.

- Написал сервер на nestjs;
- Сделал API запросы;
- Сделал часть выгрузки из базы данных через typeORM;
- Встроил в проект Jiskefet и сделал документацию в swagger;
- Подключил дополнительные html;

# Jiskefet API <!-- omit in toc -->

[![Build Status](https://travis-ci.com/SoftwareForScience/jiskefet-api.svg?branch=master)](https://travis-ci.com/SoftwareForScience/jiskefet-api)
[![codecov](https://codecov.io/gh/SoftwareForScience/jiskefet-api/branch/master/graph/badge.svg)](https://codecov.io/gh/SoftwareForScience/jiskefet-api)

## Description <!-- omit in toc -->

This bookkeeping system is a system for A Large Ion Collider Experiment
(ALICE) to keep track of what is happening to the data produced by the detectors. The electric signals produced by the various detectors which
together are the ALICE detector are being reconstructed, calibrated, compressed and used in numerous but specific ways. It is important to register  
how this is done to make a reproduction of data possible and thereby a validation of the information produced. The project is also known as the
Jiskefet project.  

This is the **back-end API** for the Jiskefet project.   
The **front-end UI** can be found here: https://github.com/SoftwareForScience/jiskefet-ui  
And the **Ansible playbook** to deploy the application can be found here: https://github.com/SoftwareForScience/sfs-ansible

## Table of Contents <!-- omit in toc -->

- [Running the app for **dev**](#running-the-app-for-dev)
- [Running the app for **prod**](#running-the-app-for-prod)
- [Documentation](#documentation)
  - [NestJS](#nestjs)
  - [TypeORM](#typeorm)
    - [Database migration workflow](#database-migration-workflow)
  - [Jest (testing)](#jest-testing)

## Running the app for **dev**

```bash
$ npm install

# Copy template as .env and set your own values.
$ cp ./environments/{YOUR_ENV}.env.template .env

# Running in watch mode (nodemon)
$ npm run dev
```

## Running the app for **prod**

```bash
$ npm install

# Copy .env.dist as .env and set your own values.
$ cp .env .env.dist
```

Now the project can be run with `$ npm run start` as a background process via a node process manager, e.g. [PM2](http://pm2.keymetrics.io/).

## Documentation

### NestJS

We use NestJS as a Node.js framework. NestJS has built in
functionality to create a scalable and loosely-coupled architecture.

- [NestJS official docs](https://docs.nestjs.com/)

### TypeORM

We use TypeORM as an Object Relational Mapping tool.

- [TypeORM official docs](http://typeorm.io/)

#### Database migration workflow

Automatic migration generation creates a new migration file and writes all sql queries that must be executed to make a new database or to update the database.

To check what sql queries are going to be made when changes are made in the entities is as follows

```bash
npm run typeorm schema:log
```

To generate a migration file use the following command

```bash
npm run typeorm migration:generate -n 'name-of-migration-file'
```

The file that will be created can be found at the path chosen in ormconfig.json, the default path stated in the dist is *src/migration/*.
The rule of thumb is to generate a migration after each entity change.

To execute all pending migrations use following command

```bash
npm run typeorm migration:run
```

To revert the most recently executed migration use the following command

```bash
npm run typeorm migration:revert
```

### Jest (testing)

We use Jest for testing.

```bash
# Run tests
$ npm run test

# Open code coverage stats from latest test run
$ npm run showcoverage

# To run the benchmark first change the target url to the appropriate url. To run the benchmark use:
$ artillery run api_benchmarks.yml
```

- [Jest readme in project](test/README.md)
- [Jest official docs](https://jestjs.io/docs/en/getting-started)
